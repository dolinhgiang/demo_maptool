#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/giangdl/catkin_ws/devel_isolated/cartographer_ros:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH='/home/giangdl/catkin_ws/install_isolated/lib:/home/giangdl/catkin_ws1/install_isolated/lib:/opt/ros/melodic/lib'
export PKG_CONFIG_PATH='/home/giangdl/catkin_ws/install_isolated/lib/pkgconfig:/home/giangdl/catkin_ws1/install_isolated/lib/pkgconfig:/opt/ros/melodic/lib/pkgconfig'
export ROSLISP_PACKAGE_DIRECTORIES='/home/giangdl/catkin_ws/devel_isolated/cartographer_ros/share/common-lisp'
export ROS_PACKAGE_PATH="/home/giangdl/catkin_ws/src/cartographer_ros/cartographer_ros:/home/giangdl/catkin_ws/install_isolated/share:$ROS_PACKAGE_PATH"