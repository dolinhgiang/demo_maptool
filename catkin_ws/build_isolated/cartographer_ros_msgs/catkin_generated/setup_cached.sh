#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH='/home/giangdl/catkin_ws/devel_isolated/cartographer_ros_msgs:/home/giangdl/catkin_ws1/install_isolated:/opt/ros/melodic:/home/giangdl/catkin_ws/install_isolated'
export LD_LIBRARY_PATH='/home/giangdl/catkin_ws1/install_isolated/lib:/opt/ros/melodic/lib:/home/giangdl/catkin_ws/install_isolated/lib:/home/giangdl/catkin_ws/install_isolated/lib/x86_64-linux-gnu'
export PATH='/home/giangdl/catkin_ws1/install_isolated/bin:/opt/ros/melodic/bin:/home/giangdl/catkin_ws/install_isolated/bin:/home/giangdl/.nvm/versions/node/v12.18.3/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin'
export PKG_CONFIG_PATH='/home/giangdl/catkin_ws1/install_isolated/lib/pkgconfig:/opt/ros/melodic/lib/pkgconfig:/home/giangdl/catkin_ws/install_isolated/lib/pkgconfig:/home/giangdl/catkin_ws/install_isolated/lib/x86_64-linux-gnu/pkgconfig'
export PYTHONPATH='/home/giangdl/catkin_ws1/install_isolated/lib/python2.7/dist-packages:/opt/ros/melodic/lib/python2.7/dist-packages:/home/giangdl/catkin_ws/install_isolated/lib/python2.7/dist-packages'
export ROSLISP_PACKAGE_DIRECTORIES='/home/giangdl/catkin_ws/devel_isolated/cartographer_ros_msgs/share/common-lisp'
export ROS_PACKAGE_PATH="/home/giangdl/catkin_ws/src/cartographer_ros/cartographer_ros_msgs:$ROS_PACKAGE_PATH"